function [x A] = RL(t, b, m)
    %
    % Regresión lineal para determinar los coeficientes del polinomio
    %
    % p(t) = c_1 + c_2 * t + c_3 * t^2 + ... + c_m * t^(m-1)
    %
    % que minimice el error de los datos en el vector b, es decir
    % que la suma de los errores en el vector de tiempo k-ésimo t
    % (1/2) * sum_(k=1)^n (p(t_k) - b_k)^2 sea mínima.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - t: vector columna de dimensión n tal que t(k) < t(k+1)
    %      para todo k = 1, ..., n-1.
    % - b: vector columna de dimensión n.
    % - m: número natural mayor o igual a dos. El grado del
    %      polinomio que se necesita es menor o igual a m-1.
    %      Se tiene que m <= n.
    %
    % Out:
    % - x: vector columna de dimensión m con los coeficientes
    %      del polinomio en forma ascendente.
    % - A: matriz de Vandermonde asociada al problema.
    %
    n = length(t);
    if m > n
        m = n;
    end

    %
    % Matriz de Vandermonde
    %
    A = vander(t);
    A = fliplr(A);
    A = A(:, 1:m);

    %
    % Solución
    %
    x = MC(A, b);
end
