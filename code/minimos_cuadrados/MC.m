function [x] = MC(A, b)
    %
    % Minimización de cuadrados lineales.
    %
    % min ||Ax - b|| s.a. x in R^n
    %
    % con A in R in R^mxn n <= m, rango(A) = n
    % se calcula por medio de la factorización QR.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - A: matriz de Vandermonde o Jacobiana.
    % - b: vector columna de dimensión m.
    %
    % Out:
    % - x: vector de orden n que aproxima al minimo local.
    %
    n     = length(b);
    [Q R] = qr(A);
    Q1    = Q(:, 1:n);
    x     = R\Q1'*b;
end
