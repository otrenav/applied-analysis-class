function [x, i, res, rc, tiempo, fx, norm_gfx] = GN(...
        fname_vec, fname_scl, x, salida)
    %
	% Mínimos cuadrados no lineales a través de regresión lineal para
    % encontrar una dirección de desceno y despúes utilizar búsqueda
    % de línea.
    %
    % Teoría en Capítulo X, Nocedal, Numerical Optimization,
    % 1999, Springer Series, Estados Unidos.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
	% In:
	% - fname_vec: cadena con el nombre de la función que
    %              regresa el vector de residuales.
    % - fname_scl: cadena con el nombre de la función que
    %              regresa norma(residuales)^2/2.
	% - x:         punto inicial en R^p.
    % - salida:    variable booleana:
    %              salida == 'verbose':    imprimir resultados
    %              salida == 'no_verbose': no imprimir resultados
    %
	% Out:
	% - x:         aproximación final al mínimo local.
    % - i:         número natural con la cantidad de iteraciones.
    %

    res  = 'No optimo';
    tol  = 1e-4;
    mu   = 1/2;
    i    = 1;
    imax = 50;
    J    = jacob(fname_vec, x);
    r    = feval(fname_vec, x);
    gfx  = J'*r;
    n    = length(x);
    m    = length(r);
    t    = linspace(0, 2*pi, m);

    tic;
    while norm(gfx) > tol && i < imax
        r = feval(fname_vec, x);
        J = jacob(fname_vec, x);
        if rank(J) < n
            B = J'*J + mu*eye(n);
            d = B\(-J'*r);
        else
            d = MC(J, -r);
        end
        gfx  = J'*r;
        pend = gfx'*d;
        alfa = backtracking(fname_scl, x, d, pend);
        x    = x + alfa*d;
        i    = i + 1;
    end
    tiempo = toc;

    %
    % Resultados
    %
    fx = feval(fname_scl, x);
    J  = jacob(fname_vec, x);
    norm_gfx = norm(gfx);
    if norm(gfx) <= tol
       res = 'Optimo';
    end
    if rank(J) < n
        rc = 'No';
    else
        rc = 'Si';
    end
    if strcmp(salida, 'verbose')
        display(sprintf('\n[+] Resultado \t\t\t %s', res));
        display(sprintf('[+] Rango completo \t\t %s', rc));
        display(sprintf('[+] Iteraciones \t\t %d ', i));
        display(sprintf('[+] Tiempo (seg) \t\t %g ', tiempo));
        display(sprintf('[+] f(x) \t\t\t\t %g', fx));
        display(sprintf('[+] norm(grad(f(x))) \t %g\n', norm_gfx));
        if n > 10
            display(sprintf('[+] Aprox. de x (primeras 5 variables)\n'));
        else
            display(sprintf('[+] Aprox. de x \n'));
        end
        for j = 1:min(5, length(x))
            display(sprintf('\tx(%d) = \t\t\t\t %g', j, x(j)));
        end
        display(sprintf(' '));
    else
        if strcmp(res, 'Optimo')
            res = 'Si';
        else
            res = 'No';
        end
    end
end
