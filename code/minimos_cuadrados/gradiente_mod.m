function [gfx] = gradiente_mod(fname, x)
    %
    % Versión modificada del gradiente de acuerdo a Nocedal
    % pg. 246. La modificación está hecha para utilizar este
    % gradiente_mod en regresión no lineal.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - fname: cadena de caracteres que codifica la funcion f.
    % - x:     vector de orden n que representa el punto inicial.
    %
    % Out:
    % - gfx:   gradiente modificado de la función en fname
    %          evaluada en x.
    %
    J = jacob(fname, x);
    gfx = J'*feval(fname, x);
end
