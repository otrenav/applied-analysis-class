function [H] = hessiana_mod(fname, x)
    %
    % Versión modificada del gradiente de acuerdo a Nocedal
    % pg. 246. La modificación está hecha para utilizar este
    % gradiente_mod en regresión no lineal.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - fname: cadena de caracteres que codifica la funcion f.
    % - x:     vector de orden n que representa el punto inicial.
    %
    % Out:
    % - H:     Hessiana modificiada de la función fname evaluada
    %          en el punto x.
    %
    mu = 1/2;
    n  = length(x);
    J  = jacob(fname, x);
    H  = J'*J;
    if rank(J) < n
        H = H + eye(n)*mu;
        display(sprintf('\n[!] Se hizo ajuste a H.'));
    end
end
