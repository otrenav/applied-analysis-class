function [x, i] = RC(fname, x, delta_0, iter)
    %
	% Región de confinaza (RC) con doblez.
    %
    % Internamente, cuando B no es definida positiva se utiliza:
    % B = Hessiana(f(x)) + alpha*I_n con alpha en (-alpha, -2*-alpha).
    %
    % Teoría en Capítulo IV, Nocedal, Numerical Optimization,
    % 1999, Springer Series, Estados Unidos.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
	% In:
	% - fname: Cadena con el nombre de la función a minimizar.
	% - x:     Punto inicial en R^p.
	% - delta: Primera aproximación del radio de la RC.
    % - iter:  variable booleana:
    %          iter > 1 => Mostrar progreso cada iter iteraciones.
    %
	% Out:
    % - x:     Aproximación final al mínimo local.
    % - i:     número natural que indica la cantidad de iteraciones.
    %
    delta = delta_0;
    tol   = 1e-4;
    i     = 1;
    imax  = 10000;
    res   = 'No optimo';
    eta   = 1e-1;  % Recomendación: rho en (0, 1/4]

    tic;
    norma = norm(gradiente(fname, x));
   	while norma > tol && i < imax
		B = hessiana(fname, x);
		min_valpropio = min(eig(B));
		if(min_valpropio < 0)
			% B no es p.d., la transformamos (Nocedal pg. 75)
			B = definida_positiva(B, min_valpropio);
		end

        % Calculamos el sub-problema de RC (Nocedal pg. 72)
        % y calculamos rho (Nocedal pg. 68).
        gfx = gradiente(fname, x);
        p   = RC_sub(B, gfx, delta);
        m0  = feval(fname, x);
        mp  = feval(fname, x) + gfx'*p + 1/2*(p'*B*p);
        rho = (feval(fname, x) - feval(fname, x + p))/(m0 - mp);

		% Actualizamos RC (Nocedal pg. 69)
		if rho < 1/4
			delta = 1/4*delta;
		elseif rho > 3/4 && norm(p) == delta
			delta = min(delta_0, 2*delta);
		end
		if rho > eta
			x = x + p;
		end

        % Información de iteraciones
        x     = real(x);
        fx    = feval(fname, x);
        norma = norm(gradiente(fname, x));
        if iter >= 1 && mod(i, iter) == 0
            display(sprintf('\n[+] Iteracion \t\t\t %d', i));
            display(sprintf('[+] x(1) \t\t\t\t %gfx', x(1)));
            display(sprintf('[+] f(x) \t\t\t\t %gfx', fx));
            display(sprintf('[+] norm(grad(f(x)) \t %gfx', norma));
        end
		i = i + 1;
	end
    tiempo = toc;
    gfx = gradiente(fname, x);
	if norm(gfx) <= tol
	   res = 'Optimo';
	end

    %
    % Resultados
    %
    n  = length(x);
    fx = feval(fname, x);
    display(sprintf('\n[+] Resultado \t\t\t %s', res));
    display(sprintf('[+] Iteraciones \t\t %d ', i));
    display(sprintf('[+] Tiempo (seg) \t\t %g ', tiempo));
    display(sprintf('[+] f(x) \t\t\t\t %g', fx));
    display(sprintf('[+] norm(grad(f(x))) \t %g\n', norm(gfx)));
    if n > 10
        display(sprintf('[+] Aprox. de x (primeras 5 variables)\n'));
    else
        display(sprintf('[+] Aprox. de x \n'));
    end
    for j = 1:min(5, length(x))
        display(sprintf('\tx(%d) = \t\t\t\t %g', j, x(j)));
    end
    display(sprintf(' '));
end
