function [p] = doblez(B, g, delta)
    %
    % Función de doblez para el problema:
    %
    % min 1/2*p'*B*p + g'*p  s.a. ||p||_2 <= delta
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - B:
    % - g:
    % - delta:
    %
    % Out:
    % - p:
    %

    %
    % TODO: Terminar
    %

    % Resolver B * p = -g
    pN = B\(-g);

    if norm(pN) <= delta
        p = pN;
    else
        pC = -(g'*g/(g'*B*g))*g;
        if norm(pC) >= delta
            p = delta*pC/norm(pC);
        else
            a  = (pC - pN)'*(pC - pN);
            b  = 2*pC'*(pC - pN);
            c  = pC'*pC - delta^2;
            r  = roots([a b c]);
            rs = max(r);
            p  = pC + rs*(pN - pC);
            disp('Doblez');
        end
    end
end
