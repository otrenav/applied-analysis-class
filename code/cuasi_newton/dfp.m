function [x, i] = DFP(fname, x)
    %
    % M�todo cuasi-Newton con actualizaci�n DFP a la inversa
    % de la Hessiana y con b�squeda de l�nea para fname
    % iniciando en x.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Oto�o 2014
    % ITAM
    %
    % In:
    % - fname: cadena de caracteres que codifica la funcion f.
    % - x:     vector de orden n que representa el punto inicial.
    %
    % Out:
    % - x:     vector de orden n que aproxima al minimo local.
    % - i:     n�mero natural que indica la cantidad de iteraciones.
    %

    %
    % TODO: Arreglar
    %

    tol  = 1e-5;
    imax = 50;
    i    = 0;
    gfx  = gradiente(fname,x);
    n    = length(x);
    B    = eye(n);
    disp('iter                  || gfx ||   ')
    disp('------------------------------------------')
    while norm(gfx) > tol && i < imax
        % p = -B\gfx;
        p = -B*gfx;

        %
        % B�squeda de l�nea
        %
        j    = 0;
        jmax = 50;
        alfa = 1.0;
        fx = feval(fname,x);
        while (feval(fname, x + alfa*p) > fx + (1e-04)*alfa*gfx'*p) && j < jmax
            alfa = (0.5)*alfa;
            j = j + 1;
        end
        if j == jmax
            alfa = 1.0;
        end
        %
        % Fin de b�squeda de linea
        %

        % Se obtiene alfa en (0, 1]
        xn   = x + alfa*p;
        gfxn = gradiente(fname,xn);
        s    = xn - x;
        y    = gfxn -gfx;

        % Nueva matriz
        if y'*s > 0
           v = B*y;
           B = B + (s*s')/(y'*s) - (v*v')/(y'*v);
        else
            B = eye(n);
        end
        x   = xn;
        gfx = gfxn;
        i   = i + 1;
        disp(sprintf('%2.0f  %2.12f', i, norm(gfx) ))
    end
    xf = x;
end










