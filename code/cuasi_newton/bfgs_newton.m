function [x, fx, k, res] = BGFS_Newton(fname, x)
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - fname:
    % - x:
    %
    % Out:
    %
    % - x:
    % - fx:
    % - k:
    % - res:
    %

    %
    % TODO: Arreglar
    %

    tol  = 1e-5;
    imax = 50;
    k    = 0;
    n    = length(x);
    gfx  = gradiente(fname, x);
    H    = hessiana(fname, x);
    B    = eye(n);
    i    = 1;
    res  = 'No optimo';
    tic;
    norma = norm(gradiente(fname, x));
    display(sprintf('\n[+] Metodo de Newton BFGS ha comenzado.'));

    while norma > tol && k < imax
        Pn   = direccion_newton(B, gfx);

        % Búsqueda de linea con backtracking
        pend = gfx' * Pn;
        alfa = paso_atras(fname, x, Pn, pend);

        xn   = x + alfa*Pn;
        gfxn = gradiente(fname, xn);
        s    = xn - x;
        y    = gfxn - gfx;

        % Nueva matriz
        B   = B + (y*y')/(y'*s) - B*s*s'*B/(s'*B*s);
        x   = xn;
        gfx = gfxn;
        gfx = gradiente(fname, x);
        H   = hessiana(fname, x);
        k   = k + 1;

        % Información de iteraciones
        x     = real(x);
        fx    = feval(fname, x);
        norma = norm(gradiente(fname, x));
        if k >= 1 && mod(k, i) == 0
            display(sprintf('\n[+] Iteracion \t\t\t %d', k));
            display(sprintf('[+] x(1) \t\t\t\t %g', x(1)));
            display(sprintf('[+] f(x) \t\t\t\t %g', fx));
            display(sprintf('[+] norm(grad(f(x)) \t %g', norma));
        end
        k = k + 1;
    end
    tiempo = toc;
    if norm(gradiente(fname, x)) <= tol
       res = 'Optimo';
    end

    %
    % Resultados
    %
    n  = length(x);
    fx = feval(fname, x);
    display(sprintf('\n[+] Resultado \t\t\t %s', res));
    display(sprintf('[+] Iteraciones \t\t %d ', k));
    display(sprintf('[+] Tiempo (seg) \t\t %g ', tiempo));
    display(sprintf('[+] f(x) \t\t\t\t %g', fx));
    display(sprintf('[+] norm(grad(f(x))) \t %g\n', fx));
    if n > 10
        display(sprintf('[+] Aprox. de x (primeras 5 variables)\n'));
    else
        display(sprintf('[+] Aprox. de x \n'));
    end
    for j = 1:min(5, length(x))
        display(sprintf('\tx(%d) = \t\t\t\t %g', j, x(j)));
    end
    display(sprintf(' '));
end


