function [x, i] = BFGS(fname, x)
    %
    % Metodo de b�squeda de l�nea donde la direcci�n de descenso
    % se calcula por medio de una matriz sim�trica definda positiva
    % cuya actualizaci�n sigue el esquema cuasi-Newton de BFGS.
    %
    % Los sistemas lineales B*p = -gfx se resuelven utilizando
    % el m�todo de gradiente conjugado. En el m�todo de b�squeda
    % de l�nea se utiliza interpolaci�n de grado dos.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Oto�o 2014
    % ITAM
    %
    % In:
    % - fname: cadena con el nombre de la funcion.
    % - x:     vector de orden n que representa el punto inicial.
    % Out:
    % - x:     vector de orden n que aproxima al minimo local.
    % - i:     n�mero natural que indica la cantidad de iteraciones.
    %

    %
    % TODO: Arreglar
    %

    tol  = 1e-5;
    n    = length(x);
    gfx  = gradiente(fname,x);
    B    = eye(n);
    i    = 0;
    imax = 50;
    jmax = 50; % Para paso_atras (backtracking)

    disp('iter       norma gradiente')
    disp('---------------------------------')

    while (norm(gfx) > tol  && i < imax)
        % p = GC(B, -gfx); % Gradiente conjugado
        p = -B\gfx;

        %
        % B�squeda de l�nea
        %
        j  = 0;
        t  = 1.0;
        fx = feval(fname,x);

        % TODO: Generalizar con c1 y c2 de Wolfe.
        while (feval(fname, x + t*p) > fx + (1e-04)*t*gfx'*p ) && j < jmax
            t = 0.5*t;
            j = j + 1;
        end
        %
        % Fin de b�squeda de l�nea
        %

        if j == jmax
            xp = x + p;
        else
            xp = x + t*p;
        end

        gfxp = gradiente(fname, xp);
        s    = xp - x ;
        y    = gfxp - gfx;
        if y'*s > 0
            v = B*s;
            B = B + (y*y')/(y'*s) - (v*v')/(s'*v);
        else
            B = eye(length(x));
        end
        x   = xp;
        gfx = gfxp;
        i   = i + 1;
        disp(sprintf('%2.0f   %2.14f ', i,  norm(gfx)))
    end
end
