function [x, i] = BL(fname, x, direccion)
    %
    % Metodo de búsqueda de linea con opción de elegir entre
    % las direcciones de máximo descenso o de Newton.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - fname:     cadena de caracteres que codifica la funcion f.
    % - x:         vector de orden n que representa el punto inicial.
    % - direccion: tipo de direccion:
    %       direccion == 'max_descenso': dirección de máximo descenso.
    %       direccion == 'newton':       dirección de Newton.
    %
    % Out:
    % - x:     Aproximación final al mínimo local.
    % - i:     número natural que indica la cantidad de iteraciones.
    %
    res  = 'No optimo';
    tol  = 1e-4;
    i    = 1;
    imax = 100000;
    gfx  = gradiente(fname, x);
    tic;
    while norm(gfx) > tol && i < imax
        if strcmp(direccion, 'max_descenso')
            d = -gfx;
        elseif strcmp(direccion, 'newton')
            H = hessiana(fname, x);
            d = direccion(H, gfx);
        else
            display(sprintf('\n[!] Error de dirección.'));
        end
        pend = gfx'*d;
        alfa = backtracking(fname, x, d, pend);
        x    = x + alfa*d;
        gfx  = gradiente(fname, x);
        i    = i + 1;
    end
    if norm(gfx) <= tol
        res = 'Optimo';
    end
    tiempo = toc;

    %
    % Resultados
    %
    n  = length(x);
    fx = feval(fname, x);

    display(sprintf('\n[+] Resultado \t\t\t %s', res));
    display(sprintf('[+] Iteraciones \t\t %d ', i));
    display(sprintf('[+] Tiempo (seg) \t\t %g ', tiempo));
    display(sprintf('[+] f(x) \t\t\t\t %g', fx));
    display(sprintf('[+] norm(grad(f(x))) \t %g\n', norm(gfx)));
    if n > 10
        display(sprintf('[+] Aprox. de x (primeras 5 variables)\n'));
    else
        display(sprintf('[+] Aprox. de x \n'));
    end
    for j = 1:min(5, length(x))
        display(sprintf('\tx(%d) = \t\t\t\t %g', j, x(j)));
    end
    display(sprintf(' '));
end
