%
% Fijar el "path" de todos los archivos de código
%
path(path, '~/Projects/analisis-aplicado/code/busqueda_de_linea/')
path(path, '~/Projects/analisis-aplicado/code/cuasi_newton')
path(path, '~/Projects/analisis-aplicado/code/generales')
path(path, '~/Projects/analisis-aplicado/code/gradiente_conjugado')
path(path, '~/Projects/analisis-aplicado/code/minimos_cuadrados')
path(path, '~/Projects/analisis-aplicado/code/pruebas')
path(path, '~/Projects/analisis-aplicado/code/region_de_confianza')
