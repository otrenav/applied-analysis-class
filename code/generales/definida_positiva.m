function [A] = definida_positiva(A, mvp)
    %
	% Transformar una matriz para que sea positiva definida.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
	% In:
	% - A:   Matriz con valores propios negativos.
    % - mvp: Mínimo valor propio de la matriz A.
	%
    % Out:
	% - A:   Matriz con valores propios no negativos.
    %
    tol = 1e-5;
    n   = size(A);
    n   = n(1);
    if mvp < tol
        A = A + 2*abs(mvp)*eye(n);
    end
end
