function [H] = hessiana(fname, x)
    %
    % Hessiana de una función evaluada en un punto
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - fname: cadena de caracteres que codifica la funcion f.
    % - x:     vector de orden n que representa el punto inicial.
    %
    % Out:
    % - H:     Hessiana de la función fname evaluada en el punto x.
    %
    n  = length(x);
    H  = zeros(n);
    h  = 1e-5;
    f1 = feval(fname, x);
    for i = 1:n
        xi    = x;
        xi(i) = xi(i) + h;
        f2    = feval(fname, xi);
        for j = 1:i
            xj     = x;
            xj(j)  = xj(j) + h;
            f3     = feval(fname, xj);
            xij    = xi;
            xij(j) = xij(j) + h;
            f4     = feval(fname, xij);
            H(i,j) = (f1 + f4 - f2 - f3)/(h^2);
            if i ~= j
                H(j, i) = H(i, j);
            end
        end
    end
end
