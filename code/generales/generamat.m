function [A] = generamat(n, tau)
    %
    % Genera de manera aleatoria una familia de matrices
    % sim�tricas y positivas definidas de orden n.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Oto�o 2014
    % ITAM
    %
    % In:
    % - n:   n�mero natural mayor o igual a 2.
    % - tau: n�mero real en (0,1).
    %   Cercano a cero genera matrices ralas bien condicionadas.
    %   Cercano a uno genera a matrices densas mal condiconadas.
    %
    % Out:
    % - A := matriz sim�trica y definida positiva.
    %
    A = rand(n);

    % Simetrizaci�n
    A = (A + A')/2;

    % Matriz rala
    for k = 1:n
        for j = 1:n
            if(abs(A(k, j)) > tau)
                A(k, j) = 0.0;
            end
        end
    end

    % Diagonal de 1's
    for k = 1:n
        A(k, k) = 1.0;
    end

     % Condicional de la matriz
    vmin = min(eig(A));
    if vmin <= 0.0
        sigma = 1 - tau;
        A = A + (abs(vmin) + sigma)*eye(n);
    end

    % Simetrizaci�n
    A = (A + A')/2;
end
