function [L] = cholesky(A)
    %
    % Descomposición de Cholesky.
    %
    % Algoritmo de la pagina 51 de Nocedal.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - A: matriz simétrica.
    %
    % Out:
    % - L: matriz triangular superior tal que A = L'*L.
    %
    tol  = 1e-5;
    n    = size(A);
    i    = 0;
    imax = 50;
    beta = 0.001;
    mu   = min(eig(A));
    if mu > tol
        tau = 0;
    else
        tau = beta - mu;
    end
    A      = A + tau*eye(n);
    [L, p] = chol(A, 'lower');
    while p ~= 0 && i < imax
        tau    = max(2*tau, beta);
        A      = A + tau*eye(n);
        [L, p] = chol(A, 'lower');
        i      = i + 1;
    end
end
