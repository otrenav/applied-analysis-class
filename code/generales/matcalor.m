function [A,b] = matcalor(n)
    %
    % Esta funci�n genera la matriz y el lado derecho
    % del problema de calor de una pieza rectangular en
    % una malla rectangular de nxn.
    %
    % El calor en la parte superior es de 100 grados, en
    % la parte inferior es de 25 grados, y en las partes
    % laterales es de 50 grados. El n�mero de puntos donde
    % se debe aproximar el calor es n^2. El calor en el
    % punto k-�simo es el promedio del calor de sus cuatro
    % vecinos, al norte, sur, este y oeste. La numeraci�n
    % de los nodos va por renglones de izquierda a derecha
    % iniciando en la esquina superior izquierda.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Oto�o 2014
    % ITAM
    %
    % In:
    % - n: n�mero natural mayor a 1.
    %
    % Out:
    % - A: matriz nxn. El rengl�n i-�simo de A es la ecuaci�n
    %      del punto i-�simo en el arreglo rectangular.
    % - b: vector columna de orden n. La entrada i-�sima es el
    %      lado derecho de la ecuaci�n i-�sima.
    %
    n2 = n^2;
    A  = 4*eye(n2);

    % Subdiagonal
    for k = 2:n2
        A(k,k-1)= -1.0;
    end

    % Depurando la subdiagonal
    for k = 1:n-1
        A(k*n +1, k*n) = 0.0;
    end

    % Subdiagonal
    for k = 1:n2-n
        A(n+k,k) = -1.0;
    end

    A = A + (tril(A,-1))';

    % Lado derecho
    b = zeros(n2,1);

    b(1) = 150.0;
    b(n) = 150.0;

    for k = 2:(n - 1)
       b(k) = 100.0;
    end

    for k = (n+1):n:(n*(n-2) + 1)
       b(k) = 50.0;
    end

    for k = 2*n:n:(n*(n - 2))
       b(k) = 50.0;
    end

    b(n2 - n + 1) = 75.0;
    b(n2) = 75.0;

    for k = (n2 -n + 2):(n2 - 1)
       b(k) = 25.0;
    end
end
