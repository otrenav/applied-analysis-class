function [gfx] = gradiente(fname, x)
    %
    % Gradiente de una función evaluada en un punto
    % calculado por diferencias centradas.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - fname: cadena de caracteres que codifica la funcion f.
    % - x:     vector de orden n que representa el punto inicial.
    %
    % Out:
    % - gfx:   gradiente de la función en fname evaluada en x.
    %
    delta = x/1e4;
    for i = 1:length(x)
        if x(i) == 0
            % Evitar delta == 0
            delta(i) = 1e-10;
        end
        xi        = x;
        xi(i)     = x(i) + delta(i);
        f1        = feval(fname, xi);
        xi(i)     = x(i) - delta(i);
        f2        = feval(fname, xi);
        gfx(i, 1) = (f1 - f2)/(2*delta(i));
    end
end
