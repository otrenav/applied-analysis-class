function [d] = direccion(B, gfx)
    %
    % Calcula una dirección de descenso. En caso de que B sea la
    % Hessiana y gfx sea la dirección de máximo descenso, entonces
    % se regresa la dirección de Newton. En otro caso, dependerá
    % de B y gfx.
    %
    % d = -(f''(x))^(-1) * f'(x), resolviendo B * d = -gfx.
    %
    % Omar Trejo Navarro
    %
    % Análisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - B:   matriz que aproxima la Hesiana.
    % - gfx: vector que aproxima el gradiente.
    %
    % Out:
    % - d:  dirección de descenso (cuidado porque puede que
    %       no sea dirección de descenso.)
    %
    L  = cholesky(B);
    A  = triangular_inferior(L', -gfx);
    d = triangular_superior(L, A);
end

