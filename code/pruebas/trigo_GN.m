%
% Problema de cosecha de trigo
%
% Omar Trejo Navarro
%
% Analisis Aplicado
% Prof. Zeferino Parada
% Otoño 2014
% ITAM
%

datos   = [11.72 13.38 14.10 13.87 14.80 15.58 ...
           14.36 16.30 16.91 18.16 18.43 18.70 ...
           20.46 19.16 20.01 22.41 21.21 22.81 ...
           23.97 23.27 23.80 25.59 24.93 26.59];

n = length(datos);
t = [1 : n]';
e = 0.1;

% Punto inicial con base en x* de RC:
perturbacion = rand(3, 1);
perturbacion = e*perturbacion/max(perturbacion);
x = [0.0619117 40.3927 11.8562]' + perturbacion;

[x, i] = GN('trigo_residuales_vector', ...
            'trigo_residuales_escalar', x, 'verbose');

%
% Gráfica
%
r = x(1);  % Tasa de crecimiento de la producción
v = x(2);  % Cantidad máxima de producción
p = x(3);  % Producción inicial

% Modelo logistico de poblacion
pt = v ./ (1 + ((v / p) - 1) * exp(-r * t));

close all; hold on;
plot(t, datos, '.r', 'markersize', 25)
plot(t, pt, '--b', 'LineWidth', 2)
title('Cosecha de trigo', 'Fontsize', 18)
xlabel('An~o', 'Fontsize', 14)
ylabel('Cosecha de trigo', 'Fontsize', 14)
legend('Cosecha de trigo', 'Función logistica', 'Location', 'NorthWest')
