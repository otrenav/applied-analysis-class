function [fx] = prueba_easom(x)
    %
    % Función de prueba: Easom
    %
    % Mínimo:  f(pi,pi) = 1
    % Dominio: x,y en [-100, 100]
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - x: vector de orden 2 donde se evaluará la función.
    %
    % Out:
    % - fx: escalar con el valor de la función evaluada en x.
    %
    fx = -cos(x(1))*cos(x(2))*exp(-((x(1) - pi)^2 + (x(2) - pi)^2));
end
