%
% Problema de iPads
%
% Omar Trejo Navarro
%
% Analisis Aplicado
% Prof. Zeferino Parada
% Otoño 2014
% ITAM
%

datos = [03.27 04.19 07.33 04.69 09.25 11.12 15.30 11.80 ...
         17.00 14.00 22.90 19.50 14.60 14.10 26.00 16.35];

n = length(datos);
t = [1 : n]';
e = 0.1;

% Punto inicial con base en x* de RC:
perturbacion = rand(3, 1);
perturbacion = e*perturbacion/max(perturbacion);
x = [0.408136 19.2104 2.05263]' + perturbacion;

[x, i] = GN('ipads_residuales_vector', ...
            'ipads_residuales_escalar', x, 'verbose');

%
% Gráfica
%
r = x(1);  % Tasa de ventas de iPads
v = x(2);  % Cantidad máxima de ventas
p = x(3);  % Ventas iniciales

% Modelo logistico de poblacion
pt = v ./ (1 + ((v / p) - 1) * exp(-r * t));

close all; hold on;
plot(t, datos, '.r', 'markersize', 25)
plot(t, pt, '--b', 'LineWidth', 2)
title('Ventas de iPads', 'Fontsize', 18)
xlabel('Trimestre (1 = Q3 2010, 16 = Q2 2014)', 'Fontsize', 14)
ylabel('Ventas de iPads', 'Fontsize', 14)
legend('Ventas de iPads', 'Función logistica', 'Location', 'NorthWest')
