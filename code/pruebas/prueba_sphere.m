function [fx] = prueba_sphere(x)
    %
    % Función de prueba: Sphere
    %
    % Mínimo:  f(0,0) = 0
    % Dominio: x,y en [-inf, inf]
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - x: vector de orden 2 donde se evaluará la función.
    %
    % Out:
    % - fx: escalar con el valor de la función evaluada en x.
    %
    fx = x(1)^2 + x(2)^2;
end
