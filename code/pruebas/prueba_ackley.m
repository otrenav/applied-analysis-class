function [fx] = prueba_ackley(x)
    %
    % Función de prueba: Ackley
    %
    % Mínimo:  f(0,0) = 0
    % Dominio: x, y en [-5, 5]
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - x: vector de orden 2 donde se evaluará la función.
    %
    % Out:
    % - fx: escalar con el valor de la función evaluada en x.
    %
    fx = -20*exp(-0.2*sqrt(0.5*(x(1)^2 + x(2)^2))) - ...
         exp(0.5*(cos(2*pi*x(1)) + cos(2*pi*x(2)))) + ...
         20 + exp(1);
end
