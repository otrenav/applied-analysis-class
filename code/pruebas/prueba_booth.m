function [fx] = prueba_booth(x)
    %
    % Función de prueba: Booth
    %
    % Mínimo:  f(1,3) = 0
    % Dominio: x,y en [-10, 10]
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - x: vector de orden 2 donde se evaluará la función.
    %
    % Out:
    % - fx: escalar con el valor de la función evaluada en x.
    %
    fx = (x(1) + 2*x(2) - 7)^2 + (2*x(1) + x(2) - 5)^2;
end
