%
% Prueba con gráfica de Rosenbrock
%
% Omar Trejo Navarro
%
% Analisis Aplicado
% Prof. Zeferino Parada
% Otoño 2014
% ITAM
%

%
% TODO: Terminar
%

close all;
h  = 0.5;
np = 50;
x  = linspace(1 - h, 1 + h, )
y  = linspace(1 - h, 1 + h, )
z  = zeros(np);

%
% Evaluación de la función de Rosenbrock
%
for k = 1:np
    xk = x(k);
    for j = 1:np
        z(k, j) = feval('rosenbrock', [xk y(j)]');
    end
end

%
% Graficación
%
[X, Y] = meshgrid(x, y);

% surf(X, Y, Z);
mesh(X, Y, Z);
