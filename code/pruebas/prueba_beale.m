function [fx] = prueba_beale(x)
    %
    % Función de prueba: Beale
    %
    % Mínimo:  f(3,0.5) = 0
    % Dominio: x,y en [-4.5, 4.5]
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - x: vector de orden 2 donde se evaluará la función.
    %
    % Out:
    % - fx: escalar con el valor de la función evaluada en x.
    %
    fx = (1.5 - x(1) + x(1)*x(2))^2 + ...
         (2.25 - x(1) + x(1)*x(2)^2)^2 + ...
         (2.625 - x(1) + x(1)*x(2)^3)^2;
end
