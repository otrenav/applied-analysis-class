function [fx] = prueba_bukin(x)
    %
    % Función de prueba: Bukin
    %
    % Mínimo:  f(-10,1) = 0
    % Dominio: x en [-15, -5], y en [-3, 3]
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - x: vector de orden 2 donde se evaluará la función.
    %
    % Out:
    % - fx: escalar con el valor de la función evaluada en x.
    %
    fx = 100*sqrt(abs(x(2) - 0.01*x(2)^2)) + 0.01*abs(x(1) + 10);
end
