function [fx] = prueba_levi(x)
    %
    % Función de prueba: Levi
    %
    % Mínimo:  f(1,1) = 0
    % Dominio: x,y en [-10, 10]
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - x: vector de orden 2 donde se evaluará la función.
    %
    % Out:
    % - fx: escalar con el valor de la función evaluada en x.
    %
    fx = sin(3*pi*x(1))^2 + ...
         (x(1) - 1)^2*(1 + sin(3*pi*x(2))^2) + ...
         (x(2) - 1)^2*(1 + sin(2*pi*x(2))^2);
end
