%
% Problema de cosecha de trigo
%
% Omar Trejo Navarro
%
% Analisis Aplicado
% Prof. Zeferino Parada
% Otoño 2014
% ITAM
%
close all;
clear all;

datos  = [11.72 13.38 14.10 13.87 14.80 15.58 ...
          14.36 16.30 16.91 18.16 18.43 18.70 ...
          20.46 19.16 20.01 22.41 21.21 22.81 ...
          23.97 23.27 23.80 25.59 24.93 26.59];

n  = length(datos);
t  = [1 : n]';

% Parametros iniciales:
x = [0.005 20 30]';

% Puede ser con:
% - 'newton'
% - 'max_descenso'
[x, i] = BL('trigo_residuales_escalar', x, 'newton');

%
% Gráfica
%
r = x(1);  % Tasa de crecimiento de la producción
v = x(2);  % Cantidad máxima de producción
p = x(3);  % Producción inicial

% Modelo logistico de poblacion
pt = v ./ (1 + ((v / p) - 1) * exp(-r * t));

close all; hold on;
plot(t, datos, 'sq', 'LineWidth', 2)
plot(t, pt,   '--r', 'LineWidth', 2)
title('Cosecha de trigo', 'Fontsize', 18)
xlabel('An~o', 'Fontsize', 14)
ylabel('Cosecha de trigo', 'Fontsize', 14)
legend('Cosecha de trigo', 'Función logistica', 'Location', 'NorthWest')
