%
% Prueba de regresión lineal.
%
% Omar Trejo Navarro
%
% Analisis Aplicado
% Prof. Zeferino Parada
% Otoño 2014
% ITAM
%
clear all;

m  = 20;

%
% Ejemplo 1: sin(x)
%
% Cuidado con los ceros:
% t  = linspace(0, 2*pi, m);
% tt = linspace(0, 2*pi)';
% b  = sin(t)';

%
% Ejemplo 2: sin(10x)/x
%
% Cuidado con los ceros:
% t  = linspace(0.1, 2*pi, m);
% tt = linspace(0.1, 2*pi)';
% b  = sin(10*t)./t;
% b  = b';

%
% Ejemplo 3: sin(x)cos(5x)
%
t  = linspace(0, 2*pi, m);
tt = linspace(0, 2*pi)';
b  = sin(t).*cos(5*t);
b  = b';

display(sprintf('Grado \t ||Ax - b||/2'));
display(sprintf('------------------------'));
for i = 2:m
    close all;
    %
    % Regresión
    %
    [x A] = RL(t, b, i);
    s = 1/2*norm(A*x - b);
    display(sprintf('%d \t\t %f', i-1, s));

    %
    % Gráfica
    %
    ptt = x(1)*ones(100, 1);
    for k = 2:i
        ptt = ptt + x(k)*(tt).^(k-1);
    end
    hold on;
    plot(tt, ptt, 'b', 'LineWidth', 3);
    plot(t, b, '.r', 'markersize', 30);
    pause(2);
end


