%
% Omar Trejo Navarro
%
% Analisis Aplicado
% Prof. Zeferino Parada
% Otoño 2014
% ITAM
%

%
% TODO: Terminar
%

fname = 'rosenbrock';
x     = [2 2];
fx    = feval(fname, x);
g     = gradiente(fname, x);
B     = hessiana(fname, x);
delta = 10;
F     = [];
Q     = [];
V     = [];
for n = 1:30
    delta = delta/2;
    [p]   = doblez(B, g, delta);
    xp    = x + p;
    fxp   = feval(fname, xp);
    qxp   = 1/2*p'*B*p + g'*p + fx;
    F     = [F; fxp];
    Q     = [Q; gxp];
    V     = [V; delta];
end

nd = 30;
vd = [1:nd];
plot(vd, F, '--b', vd, Q, 'r');
legend('Rosebrock', 'Modelo cuadrático');
