%
% Problema de cosecha de trigo
% Proceso iterativo para medir qué tan bien se desempeña
% el algoritmo en general. Se va aumentando el factor de
% perturbación (e) para observar si se mantiene la convergencia.
%
% Omar Trejo Navarro
%
% Analisis Aplicado
% Prof. Zeferino Parada
% Otoño 2014
% ITAM
%

datos   = [11.72 13.38 14.10 13.87 14.80 15.58 ...
           14.36 16.30 16.91 18.16 18.43 18.70 ...
           20.46 19.16 20.01 22.41 21.21 22.81 ...
           23.97 23.27 23.80 25.59 24.93 26.59];
errores = [0.1 0.5 1 5 10];

n            = length(datos);
op_total     = 0;
iter_total   = 0;
tiempo_total = 0;
simulaciones = 20;

display(sprintf('\nColumnas de la tabla:'));
display(sprintf('---------------------'));
display(sprintf('Per   := Perturbacion'));
display(sprintf('Op    := Si se alcanzo o no un optimo'));
display(sprintf('Iter  := Numero de iteraciones que fueron necesarias'));
display(sprintf('Tmp   := Tiempo en segundos que tomo'));
display(sprintf('||g|| := Ultima norma del gradiente'));
display(sprintf('RC    := Si es o no de rango completo en x*\n\n'));
display(sprintf('Per \tOp \tIter \tTmp \t\t||g|| \t\tRC'));
display(sprintf('-------------------------------------------------'));

for l = 1:length(errores)
    e = errores(l);
    display(sprintf(' '));
    for k = 1:simulaciones
        %
        % Peturbación
        %
        perturbacion = rand(3, 1);
        perturbacion = e*perturbacion/max(perturbacion);
        x = [0.0619117 40.3927 11.8562]' + perturbacion;

        %
        % Método Gauss Newton
        %
        [x, iter, op, rc, tiempo, fx, norm_gfx] = GN(...
            'trigo_residuales_vector', ...
            'trigo_residuales_escalar', x, 'no_verbose');
        if strcmp(op, 'Si')
            op_total = op_total + 1;
        end
        iter_total   = iter_total + iter;
        tiempo_total = tiempo_total + tiempo;

        %
        % Información
        %
        display(sprintf('%3g \t%s \t%d \t\t%0.4e \t%0.4e \t%s', ...
                        e, op, iter, tiempo, norm_gfx, rc));

        if e == 0.1 && l == 1
            %
            % Gráfica
            %
            t = [1 : n]';
            r = x(1);  % Tasa de crecimiento de la producción
            v = x(2);  % Cantidad máxima de producción
            p = x(3);  % Producción inicial

            % Modelo logistico de poblacion
            pt = v ./ (1 + ((v / p) - 1) * exp(-r * t));

            close all; hold on;
            plot(t, datos, '.r', 'markersize', 25)
            plot(t, pt, '--b', 'LineWidth', 2)
            title('Cosecha de trigo (perturbacion = 0.1, iter = 1)', ...
                  'Fontsize', 18)
            xlabel('An~o', 'Fontsize', 14)
            ylabel('Cosecha de trigo', 'Fontsize', 14)
            legend('Cosecha de trigo', 'Función logistica', ...
                   'Location', 'NorthWest')
        end
    end
end

sim_total       = length(errores)*simulaciones;
op_promedio     = op_total/sim_total;
iter_promedio   = iter_total/sim_total;
tiempo_promedio = tiempo_total/sim_total;

display(sprintf('\nPorcentaje de optimos: \t %g%%', op_promedio*100));
display(sprintf('Iter promedio: \t\t\t %g', iter_promedio));
display(sprintf('Tiempo promedio: \t\t %g\n', tiempo_promedio));

