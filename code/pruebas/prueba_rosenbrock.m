function [fx] = prueba_rosenbrock(x)
    %
    % Funci�n de prueba: Rosenbrock Generalizada
    %
    % M�nimo:  f(1,...,1) = 0
    % Dominio: x,y en [-inf, inf]
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Oto�o 2014
    % ITAM
    %
    % In:
    % - x: vector de orden n donde se evaluar� la funci�n.
    %
    % Out:
    % - fx: escalar con el valor de la funci�n evaluada en x.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Oto�o 2014
    % ITAM
    %
    n  = length(x);
    fx = 0;
    for i = 1 : (n - 1)
        fx = fx + 100*(x(i+1) - x(i)^2)^2 + (x(i) - 1)^2;
    end
end
