function [x, i] = direcciones_coordenadas(A, b)
    %
    % Método de direcciones coordenadas para el sistema lineal
    % A*x = b, donde A es simétrica positiva definida.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - A: matriz simétrica positiva definida de orden n.
    % - b: vector columna de orden n.
    %
    % Out:
    % - x: aproximación a la solución de A*x = b.
    % - i: número natural que indica la cantidad de iteraciones.
    %
    n    = length(b);
    x    = zeros(n, 1);
    r    = A*x - b;
    i    = 1;
    tol  = 1e-5;
    imax = 50*n;

    while norm(r) > tol && i < imax
        k = mod(n, i);
        if k == 0
            k = n;
        end
        alfa  = -r(k)/A(k, k);
        xn    = x;
        xn(k) = xn(k) + alfa;
        x     = xn;
        r     = A*x - b;
        i     = i + 1;
    end
end
