function [x, i] = direcciones_conjugadas(A, b)
    %
    % Método de direcciones conjugadas para el sistema lineal
    % A*x = b, donde A es simétrica positiva definida.
    %
    % Omar Trejo Navarro
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - A: matriz simétrica positiva definida de orden n.
    % - b: vector columna de orden n.
    %
    % Out:
    % - x: aproximación a la solución de A*x = b.
    % - i: número natural que indica la cantidad de iteraciones.
    %
    n = length(b);

    % Direcciones conjugadas: las columnas de
    % P son conjugadas dos a dos.
    L = chol(A)';
    P = inv(L)';

    x   = zeros(n, 1);
    r   = A*x - b;
    tol = 1e-10;
    i   = 1;
    while norm(r) > tol && i <= n
        vp   = P(:, i);
        alfa = -r'*vp/(vp'*A*vp);
        x    = x + alfa*vp;
        r    = A*x - b;
        i    = i + 1;
    end
end
