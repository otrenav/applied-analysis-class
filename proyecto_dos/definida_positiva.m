function [A] = definida_positiva(A, min_valpropio)
    %
	% Transformar una matriz a que sea positiva definida
    %
	% In:
	%  A             := Matriz con valores propios negativos
    %  min_valpropio := Mínimo valor propio de la matriz A
	%
    % Out:
	%  A_dp := Matriz con valores propios no negativos
    %
    % Luis Manuel Román García (117077)
    % Omar Trejo Navarro (119711)
    % Juan Pablo Aguilar (000000)
    %
    % Análisis Aplicado
    % Prof. Zeferino Parada García
    % ITAM, Otoño 2014
    %
    tol = 1e-5;
    n   = size(A);
    n   = n(1);
    if min_valpropio < tol
        A = A + 2*abs(min_valpropio)*eye(n);
    end
end
