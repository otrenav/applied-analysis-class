function[p] = region_confianza_sub(B, g, delta)
    %
	% Sub-problema de región de confianza (RC)
    %
	% In:
	%  B     := Hessiana de f (definida positiva)
	%  g     := Vector gradiente de f
	%  delta := Radio de RC
    %
	% Out:
	%  p     := Paso hacia el mínimo local
    %
    % Luis Manuel Román García (117077)
    % Omar Trejo Navarro (119711)
    % Juan Pablo Aguilar (000000)
    %
    % Análisis Aplicado
    % Prof. Zeferino Parada García
    % ITAM, Otoño 2014
    %
	pB = B \ -g;
	if delta^2 >= pB' * pB
	 	% RC no restrictiva => tomamos
        % dirección de máximo descenso
		p = pB;
	else
		% RC restrictiva => doblez (Nocedal pg. 75)
        %   ||pU + (tau - 1)(pB - PU)||^2 = delta^2 con tau en (0, 1)
        % es una función cuadrática de una variable. Obtenemos
        % la solución con la fórmula cuadrática general para:
        %   x = a * x^2 + b * x + c.
		pU  = -((g'*g) / (g'*B*g))*g;
		a   = (pB - pU)' * (pB - pU);
		b   = 2 * pU' * (pB - pU);
		c   = (pU' * pU) - delta^2;
		tau = (-b + sqrt(b*b - 4*a*c)) / (2*a);
		p   = pU + tau*(pB - pU);
	end
end
