function [x, fx, k, res] = region_confianza(fname, x, delta_0, iter)
    %
	% Región de confinaza (RC) con doblez
    %
    % Teoría en Capítulo IV, Nocedal, Numerical Optimization,
    % 1999, Springer Series, Estados Unidos.
    %
	% In:
	%  fname := Cadena con el nombre de la función a minimizar
	%  x     := Punto inicial en R^p
	%  delta := Primera aproximación del radio de la RC
    %  iter  := Booleana, si es cierta se muestran las iteraciones
    %
	% Out:
	%  x     := Aproximación final al mínimo local
	%  res   := Cadena de caracteres tal que
	%           res == 'Óptimo', se alcanzó un mínimo local
	%           res == 'No óptimo', no se alcanzó un mínimo local
    %
	% Internamente, cuando B no es definida positiva (d.p.) se utiliza:
    % B = Hessiana(f(x)) + alpha * I_n con alpha en (-alpha, -2 * -alpha).
    %
    % Luis Manuel Román García (117077)
    % Omar Trejo Navarro (119711)
    % Juan Pablo Aguilar (000000)
    %
    % Análisis Aplicado
    % Prof. Zeferino Parada García
    % ITAM, Otoño 2014
    %

    %
    % Parámetros
    %
    delta    = delta_0;
    tol      = 1e-5;
    max_iter = 1e3;
    k        = 1;
    res      = 'No optimo';
    eta      = 1e-1;  % Recomendación: rho en (0, 1/4]

    %
    % Algoritmo
    %
    tic;
    norma = norm(gradiente(fname, x));
    display(sprintf('\n[+] Region de confianza con doblez ha comenzado.'));
	while norma > tol && k < max_iter
		B = hessiana(fname, x);
		min_valpropio = min(eig(B));
		if(min_valpropio < 0)
			% B no es p.d., la transformamos (Nocedal pg. 75)
			B = definida_positiva(B, min_valpropio);
		end
        % Calculamos el sub-problema de RC (Nocedal pg. 72)
        % y calculamos rho (Nocedal pg. 68).
        g   = gradiente(fname, x);
        p   = region_confianza_sub(B, g, delta);
        m0  = feval(fname, x);
        mp  = feval(fname, x) + g'*p + 1/2*(p'*B*p);
        rho = (feval(fname, x) - feval(fname, x + p)) / (m0 - mp);

		% Actualizamos RC (Nocedal pg. 69)
		if rho < 1/4
			delta = 1/4 * delta;
		elseif rho > 3/4 && norm(p) == delta
			delta = min(delta_0, 2 * delta);
		end
		if rho > eta
			x = x + p;
		end

        % Información de iteraciones
        x     = real(x);
        fx    = feval(fname, x);
        norma = norm(gradiente(fname, x));
        if iter >= 1 && mod(k, iter) == 0
            display(sprintf('\n[+] Iteracion \t\t\t %d', k));
            display(sprintf('[+] x(1) \t\t\t\t %g', x(1)));
            display(sprintf('[+] f(x) \t\t\t\t %g', fx));
            display(sprintf('[+] norm(grad(f(x)) \t %g', norma));
        end
		k = k + 1;
	end
    tiempo = toc;
	if norm(gradiente(fname, x)) <= tol
	   res = 'Optimo';
	end

    %
    % Resultados
    %
    n  = length(x);
    fx = feval(fname, x);
    display(sprintf('\n[+] Resultado \t\t\t %s', res));
    display(sprintf('[+] Iteraciones \t\t %d ', k));
    display(sprintf('[+] Tiempo (seg) \t\t %g ', tiempo));
    display(sprintf('[+] f(x) \t\t\t\t %g', fx));
    display(sprintf('[+] norm(grad(f(x))) \t %g\n', fx));
    if n > 10
        display(sprintf('[+] Aprox. de x (primeras 5 variables)\n'));
    else
        display(sprintf('[+] Aprox. de x \n'));
    end
    for i = 1:min(5, length(x))
        display(sprintf('\tx(%d) = \t\t\t\t %g', i, x(i)));
    end
    display(sprintf(' '));
end
