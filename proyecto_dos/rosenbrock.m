function [fx] = prueba_rosenbrock(x)
    %
    % Funci�n de prueba: Rosenbrock Generalizada
    %
    % M�nimo:  f(1,...,1) = 0
    % Dominio: x,y en [-inf, inf]
    %
    % In:
    %   x := punto donde se evalua la funci�n (R^n)
    %
    % Out:
    %   fx := funci�n envaluada en x
    %
    % Luis Manuel Rom�n Garc�a (117077)
    % Omar Trejo Navarro (119711)
    % Juan Pablo Aguilar (000000)
    %
    % An�lisis Aplicado
    % Prof. Zeferino Parada Garc�a
    % ITAM, Oto�o 2014
    %
    n  = length(x);
    fx = 0;
    for i = 1 : (n - 1)
        fx = fx + 100*(x(i+1) - x(i)^2)^2 + (x(i) - 1)^2;
    end
end
