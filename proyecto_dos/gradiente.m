function [g] = gradiente(fname, x)
    %
    % Gradiente de una función evaluada en un punto
    %
    % In:
    %  fname := Cadena de caracteres del nombre de la funcion
    %  x     := Punto donde se evaluara el gradiente de la funcion
    %
    % Out:
    %  g     := Gradiente de la función evaluada en x
    %
    % Luis Manuel Román García (117077)
    % Omar Trejo Navarro (119711)
    % Juan Pablo Aguilar (000000)
    %
    % Análisis Aplicado
    % Prof. Zeferino Parada García
    % ITAM, Otoño 2014
    %
    delta = x / 1e4;
    for i = 1:length(x)
        if x(i) == 0
            % Evitar delta == 0
            delta(i) = 1e-10;
        end
        xi     = x;
        xi(i)  = x(i) + delta(i);
        f1     = feval(fname, xi);
        xi(i)  = x(i) - delta(i);
        f2     = feval(fname, xi);
        g(i,1) = (f1 - f2) / (2 * delta(i));
    end
end
