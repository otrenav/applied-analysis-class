%
% Proyecto 2 - Problema de trigo
%
% Luis Manuel Román García (117077)
% Omar Trejo Navarro (119711)
% Juan Pablo Aguilar (000000)
%
% Análisis Aplicado
% Prof. Zeferino Parada García
% ITAM, Otoño 2014
%
close all;
clear all;

datos  = [11.72 13.38 14.10 13.87 14.80 15.58 ...
          14.36 16.30 16.91 18.16 18.43 18.70 ...
          20.46 19.16 20.01 22.41 21.21 22.81 ...
          23.97 23.27 23.80 25.59 24.93 26.59];

n  = length(datos);
t  = [1 : n]';

% Parametros iniciales:
x = [0.005 20 30]';

[x, fx, k, res] = region_confianza('trigo_res', x, 10, 0);
res_cuad  = ipads_res(x);

%
% Gráfica
%
r = x(1);  % Tasa de crecimiento de la producción
v = x(2);  % Cantidad máxima de producción
p = x(3);  % Producción inicial

% Modelo logistico de poblacion
pt = v ./ (1 + ((v / p) - 1) * exp(-r * t));

close all; hold on;
plot(t, datos, 'sq', 'LineWidth', 2)
plot(t, pt,   '--r', 'LineWidth', 2)
title('Cosecha de trigo', 'Fontsize', 18)
xlabel('An~o', 'Fontsize', 14)
ylabel('Cosecha de trigo', 'Fontsize', 14)
legend('Cosecha de trigo', 'Función logistica', 'Location', 'NorthWest')
