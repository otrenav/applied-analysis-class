function [H] = hessiana(fname, x)
    %
    % Hessiana de una función evaluada en un punto
    %
    % In:
    %  fname := Cadena de caracteres del nombre de la funcion
    %  x     := Punto donde se evaluara la Hessiana de la funcion
    %
    % Out:
    %  H    := Hessiana de la función evaluada en el punto
    %
    % Luis Manuel Román García (117077)
    % Omar Trejo Navarro (119711)
    % Juan Pablo Aguilar (000000)
    %
    % Análisis Aplicado
    % Prof. Zeferino Parada García
    % ITAM, Otoño 2014
    %
    n  = length(x);
    H  = zeros(n);
    h  = 1e-5;
    f1 = feval(fname, x);
    for i = 1:n
        xi    = x;
        xi(i) = xi(i) + h;
        f2    = feval(fname, xi);
        for j = 1:i
            xj     = x;
            xj(j)  = xj(j) + h;
            f3     = feval(fname, xj);
            xij    = xi;
            xij(j) = xij(j) + h;
            f4     = feval(fname, xij);
            H(i,j) = (f1 + f4 - f2 - f3) / (h^2);
            if i ~= j
                H(j,i) = H(i,j);
            end
        end
    end
end
