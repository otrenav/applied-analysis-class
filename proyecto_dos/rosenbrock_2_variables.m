%
% Proyecto 3 - Función de Rosenbrock (R^2)
%
% Luis Manuel Román García (117077)
% Omar Trejo Navarro (119711)
% Juan Pablo Aguilar (000000)
%
% Análisis Aplicado
% Prof. Zeferino Parada García
% ITAM, Otoño 2014
%
close all;
clear all;

region_confianza('rosenbrock', 1.2 * ones(2, 1), 1, 10);

