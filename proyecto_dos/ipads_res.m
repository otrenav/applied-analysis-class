function [res_cuad] = ipads_res(x)
    %
    % Suma de residuales al cuadrado
    %
    % In:
    %   x := vector de parametros iniciales [r, k, p]
    %        r := tasa de crecimiento de las ventas
    %        k := cantidad maxima de ventas esperadas
    %        p := ventas iniciales
    %
    % Out:
    %   res_cuad := suma de residuales al cuadrado
    %
    % Luis Manuel Román García (117077)
    % Omar Trejo Navarro (119711)
    % Juan Pablo Aguilar (000000)
    %
    % Análisis Aplicado
    % Prof. Zeferino Parada García
    % ITAM, Otoño 2014
    %

    datos = [03.27 04.19 07.33 04.69 09.25 11.12 15.30 11.80 ...
             17.00 14.00 22.90 19.50 14.60 14.10 26.00 16.35]';

    r = x(1);
    k = x(2);
    p = x(3);
    n = length(datos);
    t = [1 : n]';

    % Modelo logístico de población
    m        = k ./ (1 + (k/p - 1) * exp(-r * t));

    res      = m - datos;
    res_cuad = 1/2 * res' * res;
end
