function [alfa] = backtracking(fname, x, p, pend)
    %
    % Método de paso hacia atrás (backtracking).
    %
    % Omar Trejo Navarro   119711
    % Luis M. Román García 117077
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - fname: cadena de caracteres que codifica la funcion f.
    % - x:     vector de orden n que representa el punto de descenso.
    % - p:     vector de orden n con dirección de descenso.
    % - pend:  número real con la derivada direccional de fname
    %          en x a lo largo de de la dirección de descenso.
    %
    % Out:
    % - alfa:  número real entre [0,1] donde x + alfa*p cumple
    %          las condiciones de Wolfe.
    %

    % Recomendacion para Newton en
    % Nocedal páginas 33 y 34:
    c1 = 0.001;
    c2 = 0.9;

    i    = 0;
    imax = 100;
    alfa = 1.0;
    fx   = feval(fname, x);
    xs   = x + alfa*p;
    fxs  = feval(fname, xs);
    while (fxs > fx + c1*alfa*pend || ...
           fxs < fx + c2*alfa*pend) && ...
           i < imax
        alfa = alfa/2;
        xs   = x + alfa*p;
        fxs  = feval(fname, xs);
        i    = i + 1;
    end
end
