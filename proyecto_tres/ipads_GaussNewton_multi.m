%
% Problema de iPads
%
% Proceso iterativo para medir qué tan bien se desempeña
% el algoritmo en general. Se va aumentando el factor de
% perturbación (e) para observar si se mantiene la convergencia.
%
% Omar Trejo Navarro   119711
% Luis M. Román García 117077
%
% Analisis Aplicado
% Prof. Zeferino Parada
% Otoño 2014
% ITAM
%
datos   = [03.27 04.19 07.33 04.69 09.25 11.12 15.30 11.80 ...
           17.00 14.00 22.90 19.50 14.60 14.10 26.00 16.35];
errores = [0.1 0.5 1 5 10];

n            = length(datos);
op_total     = 0;
iter_total   = 0;
tiempo_total = 0;
simulaciones = 20;

display(sprintf('\nColumnas de la tabla:'));
display(sprintf('---------------------'));
display(sprintf('Per   := Perturbacion'));
display(sprintf('Op    := Si se alcanzo o no un optimo'));
display(sprintf('Iter  := Numero de iteraciones que fueron necesarias'));
display(sprintf('Tmp   := Tiempo en segundos que tomo'));
display(sprintf('||g|| := Ultima norma del gradiente'));
display(sprintf('RC    := Si es o no de rango completo en x*\n\n'));
display(sprintf('Per \tOp \tIter \tTmp \t\t||g|| \t\tRC'));
display(sprintf('-------------------------------------------------'));

for l = 1:length(errores)
    e = errores(l);
    display(sprintf(' '));
    for k = 1:simulaciones
        %
        % Peturbación
        %
        perturbacion = rand(3, 1);
        perturbacion = e*perturbacion/max(perturbacion);
        x = [0.408136 19.2104 2.05263]' + perturbacion;

        %
        % Método Gauss Newton
        %
        [x, iter, op, rc, tiempo, fx, norm_gfx] = GaussNewton(...
            'ipads_residuales_vector', ...
            'ipads_residuales_escalar', x, 'no_verbose');
        if strcmp(op, 'Si')
            op_total = op_total + 1;
        end
        iter_total   = iter_total + iter;
        tiempo_total = tiempo_total + tiempo;

        %
        % Información
        %
        display(sprintf('%3g \t%s \t%d \t\t%0.4e \t%0.4e \t%s', ...
                        e, op, iter, tiempo, norm_gfx, rc));

        if e == 0.1 && l == 1
            %
            % Gráfica
            %
            t = [1 : n]';
            r = x(1);  % Tasa de crecimiento de la producción
            v = x(2);  % Cantidad máxima de producción
            p = x(3);  % Producción inicial

            % Modelo logistico de poblacion
            pt = v ./ (1 + ((v / p) - 1) * exp(-r * t));

            close all; hold on;
            plot(t, datos, '.r', 'markersize', 25)
            plot(t, pt, '--b', 'LineWidth', 2)
            title('Ventas de iPads', 'Fontsize', 18)
            xlabel('Trimestre (1 = Q3 2010, 16 = Q2 2014)', ...
                   'Fontsize', 14)
            ylabel('Ventas de iPads', 'Fontsize', 14)
            legend('Ventas de iPads', 'Función logistica', ...
                   'Location', 'NorthWest')
        end
    end
end

sim_total       = length(errores)*simulaciones;
op_promedio     = op_total/sim_total;
iter_promedio   = iter_total/sim_total;
tiempo_promedio = tiempo_total/sim_total;

display(sprintf('\nPorcentaje de optimos: \t %g%%', op_promedio*100));
display(sprintf('Iter promedio: \t\t\t %g', iter_promedio));
display(sprintf('Tiempo promedio: \t\t %g\n', tiempo_promedio));
