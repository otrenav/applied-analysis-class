function [J] = jacob(fname, x)
    %
    % Calcular la matriz Jacobiana de fname en x.
    %
    % En caso de que fname sea un escalar, esta función se
    % reduce a calcular el gradiente. En caso de que la imagen
    % sea vectorial, se calcula la Jacobiana.
    %
    % Se utilizan diferencias hacia adelante.
    %
    % Omar Trejo Navarro   119711
    % Luis M. Román García 117077
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    %
    % - fname: Cadena con el nombre de la función.
    % - x:     Punto donde se calculará la Jacobiana.
    %
    % Out:
    % - J:     Matriz Jacobiana de fname en x.
    %
    rx = feval(fname, x);
    n = length(x);
    m = length(rx);
    J = zeros(m, n);
    h = 1e-6;
    for i = 1:n
        xn      = x;
        xn(i)   = xn(i) + h;
        rxn     = feval(fname, xn);
        J(:, i) = (rxn - rx)/h;
    end
end
