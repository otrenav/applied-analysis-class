function [res] = trigo_residuales_escalar(x, res)
    %
    % Suma de residuales al cuadrado del problema
    % de cosecha de trigo.
    %
    % Omar Trejo Navarro   119711
    % Luis M. Román García 117077
    %
    % Analisis Aplicado
    % Prof. Zeferino Parada
    % Otoño 2014
    % ITAM
    %
    % In:
    % - x:   vector de parametros iniciales [r, k, p].
    %        r: tasa de crecimiento de la cosecha de trigo.
    %        k: cosecha maxima de trigo esperada.
    %        p: cosecha inicial.
    %
    % Out:
    % - res: norma cuadrada de residuales entre dos (escalar).
    %

    datos  = [11.72 13.38 14.10 13.87 14.80 15.58 ...
              14.36 16.30 16.91 18.16 18.43 18.70 ...
              20.46 19.16 20.01 22.41 21.21 22.81 ...
              23.97 23.27 23.80 25.59 24.93 26.59]';

    r = x(1);
    k = x(2);
    p = x(3);
    n = length(datos);
    t = [1 : n]';

    % Modelo logístico de población
    m = k./(1 + (k/p - 1)*exp(-r*t));

    res = m - datos;
    res = 1/2*res'*res;
end
