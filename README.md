
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Applied analysis class

- Omar Trejo
- August, 2014

This code was developed for the applied mathematical analysis class that I
attended at ITAM. The code is written in Matlab and contains code, images, and
some PDFs.

The main resource for the class was:

- [Nocedal & Wright - Numerical Optimization (2006)](http://www.springer.com/us/book/9780387303031)

---

> "The best ideas are common property."
>
> —Seneca
