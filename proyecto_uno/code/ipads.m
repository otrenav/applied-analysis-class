%
% Proyecto 1 - Problema de iPads
%
% Omar Trejo Navarro - 119711
% Luis Roman Garcia  - 117077
% Fernanda Mora Alba - 103596
%
% Analisis Aplicado
% Otono 2014
% ITAM
%
close all;
clear all;

datos = [03.27 04.19 07.33 04.69 09.25 11.12 15.30 11.80 ...
         17.00 14.00 22.90 19.50 14.60 14.10 26.00 16.35];

n  = length(datos);
t  = [1 : n]';

% Parametros iniciales:
x0 = [0.01 500 3]';

[x, iter] = met_bus_lin_newton('ipads_res', x0);
res_cuad  = ipads_res(x);
display(sprintf('\n================================='));
display(sprintf('== Aproximacion a minimo local =='));
display(sprintf('== Metodo de busqueda de linea =='));
display(sprintf('================================='));
display(sprintf('Iteraciones: \t\t %d ', iter));
display(sprintf('Aproximacion: \n'));
display(sprintf('\t r  = \t\t\t %g', x(1)));
display(sprintf('\t K  = \t\t\t %g', x(2)));
display(sprintf('\t P0 = \t\t\t %g', x(3)));
display(sprintf('\nResiduales: \t\t %g', res_cuad));
display(sprintf('================================='));

%
% Gráfica
%
r = x(1);  % Tasa de ventas de iPads
v = x(2);  % Cantidad máxima de ventas
p = x(3);  % Ventas iniciales

% Modelo logistico de poblacion
pt = v ./ (1 + ((v / p) - 1) * exp(-r * t));

close all; hold on;
plot(t, datos, 'sq', 'LineWidth', 2)
plot(t, pt,   '--r', 'LineWidth', 2)
title('Ventas de iPads', 'Fontsize', 18)
xlabel('Trimestre (1 = Q3 2010, 16 = Q2 2014)', 'Fontsize', 14)
ylabel('Ventas de iPads', 'Fontsize', 14)
legend('Ventas de iPads', 'Función logistica', 'Location', 'NorthWest')
