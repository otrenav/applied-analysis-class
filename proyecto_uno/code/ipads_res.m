function [res_cuad] = ipads_res(x)
    %
    % Suma de residuales al cuadrado.
    %
    % Omar Trejo Navarro - 119711
    % Luis Roman Garcia  - 117077
    % Fernanda Mora Alba - 103596
    %
    % Analisis Aplicado
    % Otono 2014
    % ITAM
    %
    % In:
    %   x := vector de parametros iniciales [r, k, p].
    %        r := tasa de crecimiento de las ventas.
    %        k := cantidad maxima de ventas esperadas.
    %        P := ventas iniciales.
    %
    % Out:
    %   res_cuad := suma de residuales al cuadrado.
    %

    % Nota: es necesario poner los datos aqui,
    % aunque sea redundante (porque ya se pusieron
    % en el archivo principal del problema) porque
    % de otra forma se deben mandar como un parametro
    % a traves de varias funciones lo cual no es una
    % solucion elegante (tampoco esta).

    datos = [03.27 04.19 07.33 04.69 09.25 11.12 15.30 11.80 ...
             17.00 14.00 22.90 19.50 14.60 14.10 26.00 16.35]';

    r = x(1);
    k = x(2);
    p = x(3);
    n = length(datos);
    t = [1 : n]';

    % Modelo logístico de población
    m        = k ./ (1 + (k/p - 1) * exp(-r * t));

    res      = m - datos;
    res_cuad = 1/2 * res' * res;
end
