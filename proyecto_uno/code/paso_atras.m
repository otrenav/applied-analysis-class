function [alfa] = paso_atras(fname, x, p, pend)
    %
    % Metodo de paso hacia atras (backtracking).
    %
    % Omar Trejo Navarro - 119711
    % Luis Roman Garcia  - 117077
    % Fernanda Mora Alba - 103596
    %
    % Analisis Aplicado
    % Otono 2014
    % ITAM
    %
    % In:
    %   fname := cadena de caracteres que codifica la funcion f.
    %   x     := vector que representa el punto donde se desciende.
    %   p     := vector con direccion de descenso.
    %   pend  := numero real con la derivada direccional de fname
    %            en x a lo largo de del vector p.
    %
    % Out:
    %   alfa  := numero real entre [0,1] donde x + alfa * p cumple
    %            las condiciones de Wolfe.
    %

    % Recomendacion para Newton en
    % Nocdela páginas 33 y 34:
    c1      = 0.001;
    c2      = 0.9;

    iter    = 0;
    maxiter = 30;
    alfa    = 1.0;
    fx      = feval(fname, x);
    xs      = x + alfa * p;
    fxs     = feval(fname, xs);

    while(fxs > fx + c1 * alfa * pend || ...
        fxs < fx + c2 * alfa * pend) ...
        && iter < maxiter

        alfa  = alfa / 2;
        xs    = x + alfa * p;
        fxs   = feval(fname, xs);
        iter  = iter + 1;
    end
end
